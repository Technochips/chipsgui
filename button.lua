local button = class("button")

function button:initialize()
	self.label = "button"
	self.oldLabel = label
	self.style = {}
	self.func = {}
	self.x = 0
	self.y = 0
	self.width = 10
	self.height = 10
	self.autoSize = false
	
	self.isHovering = false
	self.isClicked = false
end

function button:update()
	local mx, my = love.mouse.getPosition()
	if mx > self.x and my > self.y
	and mx < self.x + self.width and my < self.y + self.height then
		if not self.isHovering and self.func.startHovering then
			self.func.startHovering()
		end
		self.isHovering = true
		if love.mouse.isDown(1, 2) then
			if not self.isClicked and self.func.pressed then
				self.func.pressed()
			end
			self.isClicked = true
		else
			if self.isClicked and self.func.click then
				self.func.click()
			end
		end
	else
		if self.isHovering and self.func.stopHovering then
			self.func.stopHovering()
		end
		self.isHovering = false
	end
	if not love.mouse.isDown(1, 2) then
		if self.isClicked and self.func.released then
			self.func.released()
		end
		self.isClicked = false
	end
end

function button:draw()
	local background_color = self.style["background-color"]
	local border_color = self.style["border-color"]
	local border_width = self.style["border-width"]
	local border_style = self.style["border-style"]
	local color = self.style["color"]
	local font_size = self.style["font-size"]
	if self.isClicked then
		if self.style["background-color-click"] ~= nil then background_color = self.style["background--color-click"] end
		if self.style["border-color-click"] ~= nil then border_color = self.style["border-color-click"] end
		if self.style["border-width-click"] ~= nil then border_width = self.style["border-width-click"] end
		if self.style["border-style-click"] ~= nil then border_style = self.style["border-style-click"] end
		if self.style["color-click"] ~= nil then color = self.style["color-click"] end
		if self.style["font-size-click"] ~= nil then font_size = self.style["font-size-click"] end
	elseif self.isHovering then
		if self.style["background-color-hover"] ~= nil then background_color = self.style["background-color-hover"] end
		if self.style["border-color-hover"] ~= nil then border_color = self.style["border-color-hover"] end
		if self.style["border-width-hover"] ~= nil then border_width = self.style["border-width-hover"] end
		if self.style["border-style-hover"] ~= nil then border_style = self.style["border-style-hover"] end
		if self.style["color-hover"] ~= nil then color = self.style["color-hover"] end
		if self.style["font-size-hover"] ~= nil then font_size = self.style["font-size-hover"] end
	end
	
	if self.oldLabel ~= self.label then
		if self.autoSize then
			self.width, self.height = self.writer:drawText(self.label, self.x, self.y, font_size, false)
		end
		self.oldLabel = self.label
	end
	
	if background_color ~= nil then
		love.graphics.setColor(background_color)
		love.graphics.rectangle("fill", self.x, self.y, self.width, self.height)
	end
	if border_color ~= nil and border_width ~= nil then
		if border_style == "solid" then
			love.graphics.setLineWidth(border_width)
			love.graphics.setColor(border_color)
			love.graphics.line(self.x, self.y, self.x + self.width, self.y, self.x + self.width, self.y + self.height, self.x, self.y + self.height, self.x, self.y)
		end
	end
	if color ~= nil then
		love.graphics.setColor(color)
		self.writer:drawText(self.label, self.x, self.y, font_size)
	end
end

return button
