local font = class('font')

function font:initialize(image)
	self.image = image
	self.quads = {}
	self.exists = {}
	self.sizex = {}
	self.data = image:getData()
end
function font:drawChar(str, x, y, scale, draw)
	if draw == nil then
		draw = true
	end
	local char = string.sub(str, 1, 1)
	local byte = string.byte(char)
	if self.quads[byte] == nil then
		local x = byte
		local y = 0
		while x > 15 do
			x = x - 16
			y = y + 1
		end
		local r, g, b, a = 0
		
		r, g, b, a = self.data:getPixel(x * 17 + 16, y * 17 + 16)
		self.exists[byte] = a > 0
		if a == 0 then
			x = 15
			y = 3
		end
		self.sizex[byte] = 0
		
		local function check(c)
			if c > 0 then
				return 1
			else
				return 0
			end
		end
		
		r, g, b, a = self.data:getPixel(x * 17 + 16, y * 17 + 3)
		self.sizex[byte] = self.sizex[byte] * 2 + check(a)
		r, g, b, a = self.data:getPixel(x * 17 + 16, y * 17 + 2)
		self.sizex[byte] = self.sizex[byte] * 2 + check(a)
		r, g, b, a = self.data:getPixel(x * 17 + 16, y * 17 + 1)
		self.sizex[byte] = self.sizex[byte] * 2 + check(a)
		r, g, b, a = self.data:getPixel(x * 17 + 16, y * 17)
		self.sizex[byte] = self.sizex[byte] * 2 + check(a)
		--print(char .. ":" .. self.sizex[byte])
		
		self.quads[byte] = love.graphics.newQuad(x * 17, y * 17, 16, 16, self.image:getDimensions())
		--print("generated")
	end
	if draw then love.graphics.draw(self.image, self.quads[byte], x, y, 0, scale) end
	return self.sizex[byte] * scale, 16 * scale
end
function font:drawText(str, ox, oy, scale, draw)
	local x = ox
	local y = oy
	local W, H = 0, 16 * scale
	local cw = 0
	for i = 1, #str do
		local char = str:sub(i, i)
		local w, h = self:drawChar(char, x, y, scale, draw)
		w = w
		h = h
		cw = cw + w
		x = x + (self.sizex[string.byte(char)] * scale)
		if char == "\n" then
			H = H + h
			cw = 0
			x = ox
			y = y + (16 * scale)
		end
		if cw > W then
			W = cw
		end
	end
	return W, H
end

return font
